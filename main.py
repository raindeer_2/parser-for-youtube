from selenium import webdriver
from selenium.webdriver.common.by import By
import time
from selenium.webdriver.common.keys import Keys
import pprint
driver = webdriver.Firefox()
driver.get("https://www.youtube.com/c/ivarlamov/videos")

time.sleep(2)
count=5000
while count>0:
    html = driver.find_element_by_tag_name('html')
    html.send_keys(Keys.END)
    count-=1
time.sleep(2)
video_title = driver.find_elements(By.ID, "video-title")
video_views = driver.find_elements(By.ID, "metadata-line")
video_title_list = []
video_views_list = []
for i in range(len(video_title)):
    video_title_list.append(video_title[i].text)

for j in range(len(video_views)):
    video_views_list.append(video_views[j].text)
dictionary = dict(zip(video_title_list, video_views_list))
pprint.pprint(dictionary)
